# Mechanical
## Intro
Analysis of the mechanical components that making up the structure of the satellite
## Scope
* Structure
* Payload support
* Thermal protection
* Radiation protection
* Mounting for external parts e.g. antennae, PVs, magnetometers etc

## Assumptions
* [nU design](http://www.cubesat.org/s/cds_rev13_final2.pdf)


## Requirements
Modular design in order to achieve 1 to 6 U Satellites

## Components
* Main structure
* Board compartment and mounting
* PV mounting
* Payload compartment and mounting
* Mounting for external components
* Antenna compartment and mounting


## Related Systems
[Subsystem interface](subsystem-interface.md)
## External Links
List of links for further reading
