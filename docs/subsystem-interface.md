# Subsystem interface

## Intro
The subsystem interface is general-reusable component. The operation of that is
to connect all subsystem with a common bus.

## Scope
This component it consists of electrical, physical (hardware) and software
sub-components. The electrical sub-components means, for example the I2C bus or
CAN bus, the physical means the way that boards (sub - systems) connects to each
other for example PC-104 with SAMTEC-ESQ-126-21-G-D and the software that means
the application layer that specified sometimes from electrical sub-component e.g
the space wire (LVDS).

## Assumptions
The libresat-i is based on distributing architecture. For that reason is necessary
to use a multi-master communication bus in order each subsystem could communicate
with all others. Another assumption is to use an electrical interface that isolate
the sub systems in order to avoid ground loops or other power and communication
issues. The isolation of each sub system could be done with specific electrical
protocol like CAN bus or by using external isolation IC ,for example , with UART
electrical protocol. Other considerations is the speed of the electrical bus that is
mission specific and the redundancy of the bus that is mandated by multi - master and
distributing architecture. Another not so important as the above, the electrical
interface is to be implemented by micro controller or by external specific IC.


The physical interface is ideal to be modular in order to
add easily the PCB of sub-system to the structure. The modular physical interface it
must be scalable with a base unit of 1U cubesat. Additional, the interface must
be have the ability to accommodate the electrical interface. For example if the
choice is a multi - master I2C the physical bus must be have the ability to
accommodate 4 buses for contingency. It is desirable the interface to be connected with existing COTS sub system.

## Requirements
The summary of assumptions is listed is this section.

* Electrical interface:
 * Multi - master architecture
 * Isolated communication bus
 * Implemented by micro controller or external IC
 * Reliable, robust and tested in industry (space or automotive)
* Physical interface:
 * Modular
 * Scalable
 * Interfaces with existing COTS sub systems

## Components
List of components of the subsystem interface.
* Electrical interface
* Physical interface

## Related Systems
Pretty much with everything but most with [Structural](https://gitlab.com/librespacefoundation/libresat-i/blob/master/docs/Mechanical.md) and [Command and Control]().

## External Links
List of links for further reading
