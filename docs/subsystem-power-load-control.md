# Power load control
## Intro
Power load control is the satellite electric power module providing connection to a subsystem with the main energy storage subsystem (battery). It responsible for voltage regulation, electrical protection, control and monitoring for each subsystem connected to the sattelite main voltage bus.

![Power Load Control overview ](img/power_load_control.svg)

![Power Load Control overview ](img/power_load_control.png)

## Scope
Power load control  module is designed in two layers.

Layer 0 comprises of the critical safety components, that should by design continue to operate even if there is a failure in the control logic of the sattelite. Layer 0 components should be designed with no single point of failure and be tested to operate safely even under erroneous control signals.

Layer 0 functions are to regulate the central energy storage subsystem voltage bus to the appropriate for the connected subsystem voltage levels, while providing hardware safety mechanisms for electrical failures such as overcurrent and  overvoltage. Power control switches connect the regulated voltages to the subsystem power input, and should be normally closed, operating even without any external logic.

Layer 1 components add extra functionality to the Power load control  module, providing interconnection with the rest of the subsystems, implementing more complex safety logic, providing a power monitor and if the control system has a healthy status, provides control to the power connection switch of each regulted power output.

Power load control  module can be either an autonomus sub-subsytem or should be easily encapsulated into the design of a subsystem that needs power connection to the main energy storage subsystem (battery).

## Assumptions
 The design of the Power load control module is based on the assumption that all the susystems requiring power connection to the main power storage subsystem, operate under the same voltage rating, 5 Volts and 3.3 Volts.

  There is a provision for a custom power output but will be re-evaluated once the design of the rest subsystems has progressed.

  In order to be able to scale up the satelite design using the same Power load control module, the basic unit should have sufficient rating for the subsystem with the higher power demand.
## Requirements
Detailed requirements with links and documentation-rationale
## Components
Power Load Control module comprises of two layer components.
* Layer 0 components:
  * overcurrent protection
  * voltage regulation
  * power control
* Layer 1 components:
  * subsystem interface
  * power monitoring
  * safety logic


## Related Systems
This is a subsystem module closely related with the Electric Power System, but is reusable from all the subsystems that need power connection to the main energy storage subsystem (battery) in order to operate.
## External Links
List of links for further reading
