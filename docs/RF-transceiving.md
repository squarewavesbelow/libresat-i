# RF transceiving
## Intro
Communication subsystem
## Scope
What is the main scope of this (sub)system

## Assumptions
What we will be doing, and not doing (focus on the not doing)

## Requirements
Detailed requirements with links and documentation-rationale

## Components

### Hardware

### Software

### Physical Layer
#### Modulation
#### Forward Error Correction (FEC)
#### Synchronization

### Link Layer
#### Frame format
#### Frame Synchronization

### In-Sat communication API

## Related Systems
Links to related systems if needed
## External Links
List of links for further reading
