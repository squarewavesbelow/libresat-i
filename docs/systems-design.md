# Systems Design
Cubesats (and generally satellites) can be complicated, mission-specific, highly customized systems. In LibreSAT-i we are taking the approach of trying to abstract core system principles and subsystems of a typical cubesat satellite, making them as modular as possible, focusing on re-usability of components.

The table that follows outlines the core sub-systems of the satellite with their core functionality described. When functionality is reusable this is noted.

| Generic | Attitude determination and control | Power | Communication | Payload | Structure |
| :--------: | :--------: | :--------: | :--------: | :--------: | :--------: |
| Power load control  |   Attitude determination   | Energy harvesting | [RF transceiving](RF-transceiving.md)  | Payload interface   | [Mechanical](Mechanical.md)  |
| Logging   |  Attitude control   | Energy storage   |    |    |    |
| Subsystem interface   |    |  Power management   |    |    |   |
| Command and control   |    |    |    |    |    |


A picture from an early design session can be found here: 
![IMG_20161019_232804](/uploads/ff7f0746053d7245755d6a25ab342864/IMG_20161019_232804.jpg)
